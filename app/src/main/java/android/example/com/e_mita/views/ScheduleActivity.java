package android.example.com.e_mita.views;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.example.com.e_mita.R;
import android.example.com.e_mita.adapters.ScheduleAdapter;
import android.example.com.e_mita.data.ScheduleDbHelper;
import android.example.com.e_mita.models.Schedule;
import android.example.com.e_mita.utils.ConstantValues;
import android.example.com.e_mita.utils.MyDividerItemDecoration;
import android.example.com.e_mita.utils.RecyclerTouchListener;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class ScheduleActivity extends AppCompatActivity {

    private List<Schedule> scheduleItem = new ArrayList<>();
    private ScheduleAdapter scheduleAdapter;
    private ImageView profileImageMenu;
    private TextView noNotes;
    private SharedPreferences mPreferences;

    private ScheduleDbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        Toolbar toolbar = findViewById(R.id.toolbar_schedule);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        profileImageMenu = findViewById(R.id.profile_image_menu_schedule);
        profileImageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view);
            }
        });

        mPreferences = getSharedPreferences(ConstantValues.sharedPrefFile, MODE_PRIVATE);
        loadProfileImage();

        noNotes = findViewById(R.id.empty_notes_view);

        db = new ScheduleDbHelper(this);

        scheduleItem.addAll(db.getAllNotes());

        FloatingActionButton fab = findViewById(R.id.fab_schedule);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNoteDialog(false, null, -1);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.rv_schedule);
        recyclerView.setHasFixedSize(true);
        scheduleAdapter = new ScheduleAdapter(this, scheduleItem);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(scheduleAdapter);

        toggleEmptyNotes();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
            }

            @Override
            public void onLongClick(View view, int position) {
                showActionsDialog(position);
            }
        }));
    }

    public void loadProfileImage(){
        String profileUrl = mPreferences.getString(ConstantValues.PROFILE_IMAGE_URI, "");
        if(profileUrl.equals("")){
            profileImageMenu.setImageResource(R.drawable.ic_account_white);

        }else{
            Picasso.with(this).load(profileUrl)
                    .transform(new CropCircleTransformation())
                    .into(profileImageMenu);
        }
    }

    // Display anchored popup menu based on view selected
    private void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        // Inflate the menu from xml
        popup.inflate(R.menu.main_menu);
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_logout:
                        //sign out individual from app
                        android.support.v7.app.AlertDialog.Builder alertDialog;
                        //sign out individual from app
                        alertDialog=new android.support.v7.app.AlertDialog.Builder(ScheduleActivity.this);
                        alertDialog.setTitle("Logout Confirmation");
                        alertDialog.setMessage("Are you sure you want to logout?");
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent logout = new Intent(ScheduleActivity.this, LoginActivity.class);
                                startActivity(logout);
                                finish();

                            }
                        });
                        alertDialog.setNegativeButton("NO", null);
                        alertDialog.setCancelable(false);
                        alertDialog.show();

                        return true;
                    case R.id.action_profile:
                        //Open profile activity
                        Intent profile = new Intent(ScheduleActivity.this, ProfileActivity.class);
                        startActivity(profile);
                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    /**
     * Inserting new note in db
     * and refreshing the list
     */
    private void createNote(String note) {
        // inserting note in db and getting
        // newly inserted note id
        long id = db.insertSchedule(note);

        // get the newly inserted note from db
        Schedule n = db.getSchedule(id);

        if (n != null) {
            // adding new note to array list at 0 position
            scheduleItem.add(0, n);

            // refreshing the list
            scheduleAdapter.notifyDataSetChanged();

            toggleEmptyNotes();
        }
    }

    /**
     * Updating note in db and updating
     * item in the list by its position
     */
    private void updateNote(String note, int position) {
        Schedule n = scheduleItem.get(position);
        // updating note text
        n.setDescription(note);

        // updating note in db
        db.updateNote(n);

        // refreshing the list
        scheduleItem.set(position, n);
        scheduleAdapter.notifyItemChanged(position);

        toggleEmptyNotes();
    }

    /**
     * Deleting note from SQLite and removing the
     * item from the list by its position
     */
    private void deleteNote(int position) {
        // deleting the note from db
        db.deleteNote(scheduleItem.get(position));

        // removing the note from the list
        scheduleItem.remove(position);
        scheduleAdapter.notifyItemRemoved(position);

        toggleEmptyNotes();
    }

    /**
     * Opens dialog with Edit - Delete options
     * Edit - 0
     * Delete - 0
     */
    private void showActionsDialog(final int position) {
        CharSequence colors[] = new CharSequence[]{"Edit", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose option");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    showNoteDialog(true, scheduleItem.get(position), position);
                } else {
                    deleteNote(position);
                }
            }
        });
        builder.show();
    }

    /**
     * Shows alert dialog with EditText options to enter / edit
     * a note.
     * when shouldUpdate=true, it automatically displays old note and changes the
     * button text to UPDATE
     */
    private void showNoteDialog(final boolean shouldUpdate, final Schedule note, final int position) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
        @SuppressLint("InflateParams") View view = layoutInflaterAndroid.inflate(R.layout.schedule_dialog, null);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(ScheduleActivity.this);
        alertDialogBuilderUserInput.setView(view);

        final EditText inputNote = view.findViewById(R.id.note);
        TextView dialogTitle = view.findViewById(R.id.dialog_title);
        dialogTitle.setText(!shouldUpdate ? getString(R.string.lbl_new_note_title) : getString(R.string.lbl_edit_note_title));

        if (shouldUpdate && note != null) {
            inputNote.setText(note.getDescription());
        }
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton(shouldUpdate ? "update" : "save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                    }
                })
                .setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show toast message when no text is entered
                if (TextUtils.isEmpty(inputNote.getText().toString())) {
                    Toast.makeText(ScheduleActivity.this, "Enter schedule!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    alertDialog.dismiss();
                }

                // check if user updating note
                if (shouldUpdate && note != null) {
                    // update note by it's id
                    updateNote(inputNote.getText().toString(), position);
                } else {
                    // create new note
                    createNote(inputNote.getText().toString());
                }
            }
        });
    }

    /**
     * Toggling list and empty notes view
     */
    private void toggleEmptyNotes() {
        // you can check notesList.size() > 0

        if (db.getNotesCount() > 0) {
            noNotes.setVisibility(View.GONE);
        } else {
            noNotes.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(ScheduleActivity.this,DashboardActivity.class);
        startActivity(intent);
    }
}
