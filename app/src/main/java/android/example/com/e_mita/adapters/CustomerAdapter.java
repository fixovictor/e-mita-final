package android.example.com.e_mita.adapters;

import android.content.Context;
import android.content.Intent;
import android.example.com.e_mita.R;
import android.example.com.e_mita.models.Customer;
import android.example.com.e_mita.utils.ConstantValues;
import android.example.com.e_mita.views.MapLocations;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.ViewHolder> {

    private Context context;
    private List<Customer> customerNamesList;

    //constructor method
    public CustomerAdapter(Context context, List<Customer> customerNamesList) {
        this.context = context;
        this.customerNamesList = customerNamesList;
    }

    @NonNull
    @Override
    public CustomerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //setting the layout to be displayed when oncCreateViewHolder method is called
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_list_item, parent, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull CustomerAdapter.ViewHolder holder, int position) {

        final Customer customer = customerNamesList.get(position);
        holder.customerName.setText(customer.getCustomerName());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent customerLocation = new Intent(context, MapLocations.class);
                customerLocation.putExtra(ConstantValues.CUSOTMER_NAME, customer.getCustomerName());
                context.startActivity(customerLocation);
            }
        });
    }

    @Override
    public int getItemCount() {
        return customerNamesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView customerName;

        public LinearLayout cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            customerName = itemView.findViewById(R.id.customerName);
            cardView = itemView.findViewById(R.id.parent_layout);
        }
    }

}

