package android.example.com.e_mita.adapters;

import android.content.Context;
import android.content.Intent;
import android.example.com.e_mita.R;
import android.example.com.e_mita.views.ComplaintsActivity;
import android.example.com.e_mita.views.LocationActivity;
import android.example.com.e_mita.views.MeterReadingActivity;
import android.example.com.e_mita.views.ScheduleActivity;
import android.example.com.e_mita.views.StatisticsActivity;
import android.example.com.e_mita.views.TasksActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder>{

    private static final String TAG = "RecyclerViewAdapter";

    private ArrayList<Integer> imageTitle;
    private ArrayList<Integer> imageSrc;
    private Context mContext;

    public DashboardAdapter(Context context, ArrayList<Integer> imageNames, ArrayList<Integer> imageUrl ) {
        imageTitle = imageNames;
        imageSrc = imageUrl;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        holder.imageIcon.setImageResource(imageSrc.get(position));
        holder.imageName.setText(imageTitle.get(position));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = (String) holder.imageName.getText();
                switch(title){
                    case "Location":
                        Log.d(TAG, "onClick: clicked on: Location");

                        Intent location = new Intent(mContext, LocationActivity.class);
                        mContext.startActivity(location);
                        break;

                    case "Tasks":
                        Log.d(TAG, "onClick: clicked on: Tasks");

                        Intent tasks = new Intent(mContext, TasksActivity.class);
                        mContext.startActivity(tasks);
                        break;

                    case "Meter reading":
                        Log.d(TAG, "onClick: clicked on: Meter Reading");

                        Intent meter = new Intent(mContext, MeterReadingActivity.class);
                        mContext.startActivity(meter);
                        break;

                    case "Statistics":
                        Log.d(TAG, "onClick: clicked on: Statistics");

                        Intent statistics = new Intent(mContext, StatisticsActivity.class);
                        mContext.startActivity(statistics);
                        break;

                    case "Schedule":
                        Log.d(TAG, "onClick: clicked on: Schedule");

                        Intent schedule = new Intent(mContext, ScheduleActivity.class);
                        mContext.startActivity(schedule);
                        break;

                    case "Complaints":
                        Log.d(TAG, "onClick: clicked on: Complaints");

                        Intent complaints = new Intent(mContext, ComplaintsActivity.class);
                        mContext.startActivity(complaints);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageTitle.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView imageName;
        ImageView imageIcon;
        LinearLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            imageName = itemView.findViewById(R.id.image_title);
            imageIcon = itemView.findViewById(R.id.image_thumbnail);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}



