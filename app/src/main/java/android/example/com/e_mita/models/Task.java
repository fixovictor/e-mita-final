package android.example.com.e_mita.models;

/**
 * Created by FIXO on 04/01/2019.
 */

public class Task {

    private String taskName, completeStatus;

    public Task(String taskName, String completeStatus) {
        this.taskName = taskName;
        this.completeStatus = completeStatus;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getCompleteStatus() {
        return completeStatus;
    }

    public void setCompleteStatus(String completeStatus) {
        this.completeStatus = completeStatus;
    }
}
