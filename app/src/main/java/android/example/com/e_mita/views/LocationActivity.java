package android.example.com.e_mita.views;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.example.com.e_mita.R;
import android.example.com.e_mita.adapters.LocationAdapter;
import android.example.com.e_mita.models.Location;
import android.example.com.e_mita.utils.ConstantValues;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class LocationActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LocationAdapter locationAdapter;
    private ImageView profileImageMenu;
    private SharedPreferences mPreferences;
    private ArrayList<Location> locationNames = new ArrayList<>();
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        Toolbar toolbar = findViewById(R.id.toolbar_locations);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        profileImageMenu = findViewById(R.id.profile_image_menu_locations);
        profileImageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view);
            }
        });

        mPreferences = getSharedPreferences(ConstantValues.sharedPrefFile, MODE_PRIVATE);
        loadProfileImage();

        recyclerView = findViewById(R.id.rv_location);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        populateLocationViews();
    }

    public void loadProfileImage(){
        String profileUrl = mPreferences.getString(ConstantValues.PROFILE_IMAGE_URI, "");
        if(profileUrl.equals("")){
            profileImageMenu.setImageResource(R.drawable.ic_account_white);

        }else{
            Picasso.with(this).load(profileUrl)
                    .transform(new CropCircleTransformation())
                    .into(profileImageMenu);
        }
    }

    // Display anchored popup menu based on view selected
    private void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        // Inflate the menu from xml
        popup.inflate(R.menu.main_menu);
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_logout:
                        //sign out individual from app
                        AlertDialog.Builder alertDialog;
                        //sign out individual from app
                        alertDialog=new AlertDialog.Builder(LocationActivity.this);
                        alertDialog.setTitle("Logout Confirmation");
                        alertDialog.setMessage("Are you sure you want to logout?");
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent logout = new Intent(LocationActivity.this, LoginActivity.class);
                                startActivity(logout);
                                finish();

                            }
                        });
                        alertDialog.setNegativeButton("NO", null);
                        alertDialog.setCancelable(false);
                        alertDialog.show();

                        return true;
                    case R.id.action_profile:
                        //Open profile activity
                        Intent profile = new Intent(LocationActivity.this, ProfileActivity.class);
                        startActivity(profile);

                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }


    @Override
    public void onBackPressed() {
        Intent intent=new Intent(LocationActivity.this,DashboardActivity.class);
        startActivity(intent);
    }

    public void populateLocationViews()
    {
        progressDialog = ProgressDialog.show(LocationActivity.this,"Loading locations","Please Wait",false,false);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(ConstantValues.DEVICE_URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
               progressDialog.dismiss();

                for(int i=0; i<response.length(); i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        Location location = new Location(jsonObject.optString("location_name"));
                        locationNames.add(location);
                       // location.getLocationName(jsonObject.optString("location_name"));
                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        Toast.makeText(LocationActivity.this, "Connection error. Please refresh and ensure you have internet connection", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

                locationAdapter = new LocationAdapter(LocationActivity.this, locationNames);
                recyclerView.setAdapter(locationAdapter);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Log.e("Volley", "Error"+error.getMessage());
                progressDialog.dismiss();
                Toast.makeText(LocationActivity.this, "Connection error. Please refresh and ensure you have internet connection", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }
}
