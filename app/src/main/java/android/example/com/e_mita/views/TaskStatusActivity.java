package android.example.com.e_mita.views;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.example.com.e_mita.R;
import android.example.com.e_mita.utils.ConstantValues;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by FIXO on 06/01/2019.
 */

public class TaskStatusActivity extends AppCompatActivity {

    private CheckBox checkBox;
    private Button updateTaskStatus;
    private String taskDescription, taskCompleteValue;
    private String taskStatus;
    private AlertDialog.Builder alertDialog;
    private ImageView profileImageMenu;
    private SharedPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_status);

        Toolbar toolbar = findViewById(R.id.toolbar_tasks);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        updateTaskStatus=findViewById(R.id.updateTaskStatus);
        checkBox=findViewById(R.id.checkCompleteTask);

        //getting task value and task complete status (either zero or one) from Tasks class
        taskCompleteValue=getIntent().getStringExtra("taskStatus");
        taskDescription=getIntent().getStringExtra("description");

        //checking the checkbox if taskCompleteValue==1
        if (taskCompleteValue.contains("1"))
        {
            checkBox.setChecked(true);
        }

        updateTaskStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked())
                {
                    taskStatus="1";

                    UploadTaskStatus uploadTaskStatus=new UploadTaskStatus();
                    uploadTaskStatus.execute(taskStatus);
                }
                else
                {
                    taskStatus="0";

                    UploadTaskStatus uploadTaskStatus=new UploadTaskStatus();
                    uploadTaskStatus.execute(taskStatus,taskDescription);
                }
            }
        });

        profileImageMenu = findViewById(R.id.profile_image_menu_tasks);
        profileImageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view);
            }
        });


        mPreferences = getSharedPreferences(ConstantValues.sharedPrefFile, MODE_PRIVATE);
        loadProfileImage();
    }

    public void loadProfileImage(){
        String profileUrl = mPreferences.getString(ConstantValues.PROFILE_IMAGE_URI, "");
        if(profileUrl.equals("")){
            profileImageMenu.setImageResource(R.drawable.ic_account_white);

        }else{
            Picasso.with(this).load(profileUrl)
                    .transform(new CropCircleTransformation())
                    .into(profileImageMenu);
        }
    }

    // Display anchored popup menu based on view selected
    private void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        // Inflate the menu from xml
        popup.inflate(R.menu.main_menu);
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_logout:
                        //sign out individual from app
                        AlertDialog.Builder alertDialog;
                        //sign out individual from app
                        alertDialog=new AlertDialog.Builder(TaskStatusActivity.this);
                        alertDialog.setTitle("Logout Confirmation");
                        alertDialog.setMessage("Are you sure you want to logout?");
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent logout = new Intent(TaskStatusActivity.this, LoginActivity.class);
                                startActivity(logout);
                                finish();

                            }
                        });
                        alertDialog.setNegativeButton("NO", null);
                        alertDialog.setCancelable(false);
                        alertDialog.show();

                        return true;
                    case R.id.action_profile:
                        //Open profile activity
                        Intent profile = new Intent(TaskStatusActivity.this, ProfileActivity.class);
                        startActivity(profile);
                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    @SuppressLint("StaticFieldLeak")
    private class UploadTaskStatus extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... voids) {
            try {
                URL url=new URL(ConstantValues.TASKS_STATUS);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                //opening an output stream
                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data_to_post= URLEncoder.encode("taskStatus","UTF-8")+"="+URLEncoder.encode(taskStatus,"UTF-8")+"&"
                        + URLEncoder.encode("taskDescription","UTF-8")+"="+URLEncoder.encode(taskDescription,"UTF-8");

                bufferedWriter.write(data_to_post);

                //closing open objects
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                Log.i("Data","Message"+data_to_post);

                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String line;
                String result="";

                while((line=bufferedReader.readLine())!=null)
                {
                    result+=line;
                }

                //closing open connections
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            alertDialog=new AlertDialog.Builder(TaskStatusActivity.this);
            alertDialog.setTitle("Update status");

        }

        @Override
        protected void onPostExecute(String result) {

            alertDialog.setMessage(result);
            alertDialog.setPositiveButton("OK",null);



            try {
                alertDialog.show();

            }catch (NullPointerException e)
            {
                Toast.makeText(TaskStatusActivity.this,"No network connection",Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TaskStatusActivity.this, TasksActivity.class);
        startActivity(intent);
    }
}
