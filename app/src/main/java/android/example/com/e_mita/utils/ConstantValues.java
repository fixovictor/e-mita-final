package android.example.com.e_mita.utils;

public class ConstantValues {
    public static final int REQUEST_IMAGE = 71;
    public static final String DEVICE_URL = "http://192.168.137.115/emita/locations.php";
    public static final String LOGIN_URL = "http://192.168.137.115/emita/login.php";
    public static final int READ_TIMEOUT=15000;
    public static final int CONNECTION_TIMEOUT=10000;
    public static final String COMPLAINTS_URL="http://192.168.137.115/emita/complaints.php";
    public static final String TASKS_URL="http://192.168.137.115/emita/tasks.php";
    public static final String COMPLETED_TASKS_URL="http://192.168.137.115/emita/completedTasks.php";
    public static final String INCOMPLETE_TASKS_URL="http://192.168.137.115/emita/incompleteTasks.php";
    public static final String TASKS_STATUS = "http://192.168.137.115/emita/taskStatus.php";
    public static final String TECHNICIAN_USERNAME_URL="http://192.168.137.115/emita/gettingTechnicianName.php";
    public static final String NEW_PASSWORD_URL="http://192.168.137.115/emita/editingProfile.php";
    public static final String VIEW_CUSTOMERS_URL = "http://192.168.137.115/emita/areasInLocation.php";
    public static final String CUSTOMER_COORD_URL = "http://192.168.137.115/emita/customerCoordinates.php";
    public static final String KEY_PREF_NOTIFICATION_SWITCH = "notifications_switch";
    public static final String PROFILE_IMAGE_URI = "profile_image";
    public static final String sharedPrefFile = "com.example.android.profile";
    public static final String METER_NUMBER_SEARCH="http://192.168.137.115/emita/meterNumberSearch.php";
    public static final String METER_READING_URL="http://192.168.137.115/emita/meterReading.php";
    public static final String COORDINATES_URL="http://192.168.137.115/emita/gettingLongitudeLatitude.php";
    public static final String CUSOTMER_NAME = "customer-name";
    public static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";
    public static final int NOTIFICATION_ID = 0;
}
