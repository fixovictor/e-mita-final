package android.example.com.e_mita.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.example.com.e_mita.R;
import android.example.com.e_mita.models.Profile;
import android.example.com.e_mita.utils.ConstantValues;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class ProfileActivity extends AppCompatActivity implements KeyListener{

    private EditText name, email, designation, password;
    private ImageView imageIcon, editPassword;
    private final int PICK_IMAGE_REQUEST = 71;
    private Uri imageUri;
    private String emailText,passwordText;
    private List<Profile> technicianName;
    private ProgressDialog progressDialog;
    private AlertDialog.Builder alertDialog;
    private SharedPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = findViewById(R.id.toolbar_profile);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        name = findViewById(R.id.ev_names);
        email = findViewById(R.id.ev_email);
        designation = findViewById(R.id.ev_designation);
        password = findViewById(R.id.ev_password);
        imageIcon = findViewById(R.id.profile_image);
        editPassword = findViewById(R.id.edit_password);



        editPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent editPassword = new Intent (ProfileActivity.this, ChangePasswordActivity.class);
                startActivity(editPassword);
            }
        });

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_location_setting_layout, new SettingsFragment())
                .commit();

        android.support.v7.preference.PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        SharedPreferences sharedNotPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        //Boolean switchPrefNotification = sharedNotPreferences.getBoolean(KEY_PREF_NOTIFICATION_SWITCH, false);
        //Toast.makeText(this, switchPrefNotification.toString(), Toast.LENGTH_SHORT).show();

        //getting email address of logged on user to retrieve tasks assigned to them
        SharedPreferences sharedPreferences=getSharedPreferences("Login_Email", Context.MODE_PRIVATE);
        emailText=sharedPreferences.getString("email","");
        email.setText(emailText);

        //making email and name fields uneditable
        email.setKeyListener(null);
        name.setKeyListener(null);
        designation.setKeyListener(null);

        //retrieving username from database and setting it to name TextView
        GettingTechnicianName gettingTechnicianName=new GettingTechnicianName();
        gettingTechnicianName.execute(emailText);

        mPreferences = getSharedPreferences(ConstantValues.sharedPrefFile, MODE_PRIVATE);
        loadProfileImage();

    }

    public void loadProfileImage(){
        String profileUrl = mPreferences.getString(ConstantValues.PROFILE_IMAGE_URI, "");
        if(profileUrl.equals("")){
            imageIcon.setImageResource(R.drawable.ic_account);

        }else{
            Picasso.with(this).load(profileUrl)
                    .transform(new CropCircleTransformation())
                    .into((ImageView) findViewById(R.id.profile_image));
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(ProfileActivity.this,DashboardActivity.class);
        startActivity(intent);
    }

    public void updateUser(View view) {


    }

    public void openImage(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            imageUri = data.getData();

            Picasso.with(this).load(imageUri)
                    .transform(new CropCircleTransformation())
                    .into((ImageView) findViewById(R.id.profile_image));

            String imageURL = String.valueOf(imageUri);
            SharedPreferences.Editor preferencesEditor = mPreferences.edit();
            preferencesEditor.putString(ConstantValues.PROFILE_IMAGE_URI, imageURL);
            preferencesEditor.apply();
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class GettingTechnicianName extends AsyncTask<String,Void,String> {

        ProgressDialog pdLoading = new ProgressDialog(ProfileActivity.this);
        HttpURLConnection conn;
        URL url2 = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... params) {
            try {

                url2 = new URL(ConstantValues.TECHNICIAN_USERNAME_URL);

            } catch (MalformedURLException e) {

                pdLoading.dismiss();
                e.printStackTrace();
                return e.toString();
            }
            try {


                conn = (HttpURLConnection) url2.openConnection();
                conn.setReadTimeout(ConstantValues.READ_TIMEOUT);
                conn.setConnectTimeout(ConstantValues.CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput to true as we send and receive data
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // add parameter to our above url
                Uri.Builder builder = new Uri.Builder().appendQueryParameter("email", emailText);
                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e) {
                e.printStackTrace();
                pdLoading.dismiss();
                return e.toString();
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {
                    return("Connection error");
                }

            } catch (IOException e) {
                pdLoading.dismiss();
                e.printStackTrace();
                return e.toString();
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String result) {

            pdLoading.dismiss();
            technicianName = new ArrayList<>();


            try {


                JSONArray jArray = new JSONArray(result);

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    Profile data = new Profile();
                    name.setText(data.userName = json_data.optString("Tech_username"));
                }


            } catch (JSONException e) {
                Toast.makeText(ProfileActivity.this, "An error has occurred while loading your name. Please make sure you have internet connection", Toast.LENGTH_LONG).show();

            }


        }

    }



    //implemented methods of KeyListener

    @Override
    public int getInputType() {
        return 0;
    }

    @Override
    public boolean onKeyDown(View view, Editable text, int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyUp(View view, Editable text, int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyOther(View view, Editable text, KeyEvent event) {
        return false;
    }

    @Override
    public void clearMetaKeyState(View view, Editable content, int states) {

    }
}
