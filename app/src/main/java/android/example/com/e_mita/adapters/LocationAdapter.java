package android.example.com.e_mita.adapters;

import android.content.Context;
import android.content.Intent;
import android.example.com.e_mita.R;
import android.example.com.e_mita.models.Location;
import android.example.com.e_mita.utils.ConstantValues;
import android.example.com.e_mita.views.ViewCustomers;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder>{

    private static final String TAG = "RecyclerViewAdapter";

    private Context mContext;
    private List<Location> locationName;

    public LocationAdapter(Context context, ArrayList<Location> locationTitles) {
        locationName = locationTitles;
        mContext = context;
    }

    @Override
    public LocationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_list_item, parent, false);
        LocationAdapter.ViewHolder holder = new LocationAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final LocationAdapter.ViewHolder holder, final int position) {
        final Location location=locationName.get(position);
        holder.nameLocation.setText(location.getLocationName());


        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nameOfLocation=location.getLocationName();

                Intent intent=new Intent(mContext,ViewCustomers.class);
                intent.putExtra("locationName",nameOfLocation);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return locationName.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView nameLocation;
        LinearLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            nameLocation = itemView.findViewById(R.id.location_name);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
