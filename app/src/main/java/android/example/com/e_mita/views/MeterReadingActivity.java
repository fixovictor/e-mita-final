package android.example.com.e_mita.views;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.example.com.e_mita.BuildConfig;
import android.example.com.e_mita.R;
import android.example.com.e_mita.models.Coordinates;
import android.example.com.e_mita.models.Search;
import android.example.com.e_mita.utils.ConnectivityReceiver;
import android.example.com.e_mita.utils.ConstantValues;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.method.KeyListener;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

import static android.example.com.e_mita.utils.ConstantValues.REQUEST_IMAGE;

public class MeterReadingActivity extends AppCompatActivity implements KeyListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnMapReadyCallback,
        ConnectivityReceiver.ConnectivityReceiverListener {

    private static final String TAG = MeterReadingActivity.class.getSimpleName();
    private String longitudeValue;
    private String latitudeValue;
    private String longitude;
    private String latitude;
    private ImageView meterPicture;
    private ImageView profileImageMenu;
    private SharedPreferences mPreferences;
    private TextView dateView, monthView;
    private Bitmap bitmap;
    private EditText meterNumber, currentReading, previousReading, unitsConsumed;


    //location updates interval - 5sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    /* fastest updates interval - 3sec
    location updates will be received if another app is requesting the locations
    than your app can handle
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 3000;
    private static final int REQUEST_CHECK_SETTINGS = 100;

    //location related apis
    private SettingsClient settingsClient;
    private LocationSettingsRequest locationSettingsRequest;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private LatLng latLng;
    private GoogleMap mGoogleMap;
    private SupportMapFragment mFragment;
    private Marker mCurrLocation;

    //by Fixo
    private SearchView searchView;
    // private EditText meterNumber,currentReading,previousReading,unitsConsumed,customersID,imageName;
    // private Button updateButton,openCamera;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private List<Search> meterNumberDetails;
    private List<Coordinates> coordinateValues;
    String searchQuery;
    private AlertDialog.Builder alertDialog;
    ProgressDialog progressDialog;
    Intent intent;
    boolean check = true;
    String getImageNameFromEditText;
    String ImageNameFieldOnServer = "image_name";
    String ImagePathFieldOnServer = "image_path";
    String url, customerId, imageViewValue;
    String currentReadingText, meterNumberText, previousReadingText, customerIdText, unitsConsumedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meter_reading);

        Toolbar toolbar = findViewById(R.id.toolbar_meter);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        mFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_meter_reading);
        meterPicture = findViewById(R.id.meter_image);
        dateView = findViewById(R.id.tv_date);
        monthView = findViewById(R.id.tv_month);
        searchView = findViewById(R.id.ev_search_bar);
        meterNumber = findViewById(R.id.ev_meter_number);
        currentReading = findViewById(R.id.ev_current_reading);
        previousReading = findViewById(R.id.ev_previous_reading);
        unitsConsumed = findViewById(R.id.ev_units_consumed);

        //making units consumed and previous reading uneditable
        unitsConsumed.setKeyListener(null);
        previousReading.setKeyListener(null);

        //initializing imageViewValue to null
        imageViewValue=null;
        //setting the date
        //gatDate();
        populateViews();

        //initialize the necessary libraries
        init();

        //overriding searchView Methods
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // updateButton.setVisibility(View.VISIBLE);
                searchQuery = query;

                currentReadingText = currentReading.getText().toString();
                if (currentReadingText.isEmpty()) {
                    alertDialog = new AlertDialog.Builder(MeterReadingActivity.this);
                    alertDialog.setMessage("Enter current reading first");
                    alertDialog.setPositiveButton("OK", null);
                    alertDialog.setCancelable(false);
                    alertDialog.show();
                } else {

                    GetMeterNumber getMeterNumber = new GetMeterNumber();
                    getMeterNumber.execute(searchQuery);

                    //executing a class to get the meter location coordinates
                    GetLongitudeLatitude getLongitudeLatitude=new GetLongitudeLatitude();
                    getLongitudeLatitude.execute(searchQuery);

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//                searchQuery=newText;
//
//                GetMeterNumber getMeterNumber=new GetMeterNumber();
//                getMeterNumber.execute(searchQuery);
                return false;
            }
        });

        profileImageMenu = findViewById(R.id.profile_image_menu_meter);
        profileImageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view);
            }
        });

        mPreferences = getSharedPreferences(ConstantValues.sharedPrefFile, MODE_PRIVATE);
        loadProfileImage();

        ImageView refreshButton = findViewById(R.id.refresh_meter);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFieldstoNull();
            }
        });

    }

    private void setFieldstoNull() {
        meterNumber.setText("");
        currentReading.setText("");
        previousReading.setText("");
        unitsConsumed.setText("");
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setIconified(true);
        meterPicture.setImageResource(R.drawable.ic_add_a_photo);
        meterPicture.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
    }

    public void loadProfileImage() {
        String profileUrl = mPreferences.getString(ConstantValues.PROFILE_IMAGE_URI, "");
        if (profileUrl.equals("")) {
            profileImageMenu.setImageResource(R.drawable.ic_account_white);

        } else {
            Picasso.with(this).load(profileUrl)
                    .transform(new CropCircleTransformation())
                    .into(profileImageMenu);
        }
    }

    // Display anchored popup menu based on view selected
    private void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        // Inflate the menu from xml
        popup.inflate(R.menu.main_menu);
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_logout:
                        //sign out individual from app
                        AlertDialog.Builder alertDialog;
                        //sign out individual from app
                        alertDialog=new AlertDialog.Builder(MeterReadingActivity.this);
                        alertDialog.setTitle("Logout Confirmation");
                        alertDialog.setMessage("Are you sure you want to logout?");
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent logout = new Intent(MeterReadingActivity.this, LoginActivity.class);
                                startActivity(logout);
                                finish();

                            }
                        });
                        alertDialog.setNegativeButton("NO", null);
                        alertDialog.setCancelable(false);
                        alertDialog.show();

                        return true;
                    case R.id.action_profile:
                        //Open profile activity
                        Intent profile = new Intent(MeterReadingActivity.this, ProfileActivity.class);
                        startActivity(profile);

                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    private void populateViews() {
        Calendar c = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat("dd");
        String formattedDate = df.format(c.getTime());
        dateView.setText(formattedDate);

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat mf = new SimpleDateFormat("MMM");
        String formattedMonth = mf.format(c.getTime());
        monthView.setText(formattedMonth);

    }


    private void init() {
        settingsClient = LocationServices.getSettingsClient(this);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        locationSettingsRequest = builder.build();

        startLocationUpdates();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //outState.putString("search_no", String.valueOf(searchMeter.getText()));
        outState.putString("meter_no", String.valueOf(meterNumber.getText()));
        outState.putString("current_reading", String.valueOf(currentReading.getText()));
        outState.putString("previous_reading", String.valueOf(previousReading.getText()));
        outState.putString("units_consumed", String.valueOf(unitsConsumed.getText()));
        outState.putParcelable("BitmapImage", bitmap);
    }

    @Override
    protected void onStart() {
        super.onStart();

       /* //instantiating GoogleClient
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        // Call GoogleApiClient connection when starting the Activity
        mGoogleApiClient.connect();*/

    }

    @Override
    protected void onStop() {
        super.onStop();

        // Disconnect GoogleApiClient when stopping Activity
        // mGoogleApiClient.disconnect();
    }

    /*
     * Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */
    private void startLocationUpdates() {
        settingsClient
                .checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        //Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(MeterReadingActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(MeterReadingActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                    }
                });

        mFragment.getMapAsync(MeterReadingActivity.this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);

        buildGoogleApiClient();

        mGoogleApiClient.connect();

    }

    @Override
    public void onPause() {
        super.onPause();
        //Unregister for location callbacks:

        //remove this try catch later by implementing mGoogleClient.connect() inside onStart() method

        try {
            if (mGoogleApiClient != null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        } catch (IllegalStateException e) {
            Toast.makeText(this, "Google client is not connected", Toast.LENGTH_SHORT).show();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        //Toast.makeText(this, "buildGoogleApiClient", Toast.LENGTH_SHORT).show();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onConnected(Bundle bundle) {
        //Toast.makeText(this, "onConnected", Toast.LENGTH_SHORT).show();

        @SuppressLint("MissingPermission") Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            //place marker at current position
            mGoogleMap.clear();
            latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
            mCurrLocation = mGoogleMap.addMarker(markerOptions);

        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

        Toast.makeText(this, "onConnectionSuspended", Toast.LENGTH_SHORT).show();

        //Toast.makeText(this,"onConnectionSuspended",Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Toast.makeText(this, "onConnectionFailed", Toast.LENGTH_SHORT).show();

        //Toast.makeText(this,"onConnectionFailed",Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onLocationChanged(final Location location) {
        settingsClient
                .checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        //remove previous current location marker and add new one at current position
                        if (mCurrLocation != null) {
                            mCurrLocation.remove();
                        }
                        latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(latLng);
                        markerOptions.title("You are here");
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                        mCurrLocation = mGoogleMap.addMarker(markerOptions);

                        //Unregister the listener if one location is needed
                        //LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(MeterReadingActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(MeterReadingActivity.this, errorMessage, Toast.LENGTH_LONG).show();

                                openSettings();
                        }

                    }
                });


    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MeterReadingActivity.this, DashboardActivity.class);
        startActivity(intent);
    }

    public void openCamera(View view) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, REQUEST_IMAGE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK && data != null
                && data.getExtras() != null) {
            bitmap = (Bitmap) data.getExtras().get("data");
            meterPicture.setImageBitmap(bitmap);
            meterPicture.setScaleType(ImageView.ScaleType.FIT_XY);
            imageViewValue="1";

        }
    }

    // Modify this method to accept longitude and latitude values from the db
    public void openLocation(View view) {
        settingsClient
                .checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        //remove previous current location marker and add new one at current position
                        if (mCurrLocation != null) {
                            mCurrLocation.remove();
                        }


                        //can edit the variables to be replaced with values from db
                        double customerLatitude, customerLongitude;
                        try {
                            customerLatitude = Double.parseDouble(latitudeValue);
                            customerLongitude = Double.parseDouble(longitudeValue);
                            latLng = new LatLng(customerLatitude, customerLongitude);
                            MarkerOptions markerOptions = new MarkerOptions();
                            markerOptions.position(latLng);
                            markerOptions.title("Your destination");
                            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                            mCurrLocation = mGoogleMap.addMarker(markerOptions);

                            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MeterReadingActivity.this);

                            mGoogleMap.getUiSettings().setZoomControlsEnabled(true);

                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                            //Toast.makeText(MeterReadingActivity.this, "Longitude is "+customerLongitude+"\n Latitude is "+ customerLatitude, Toast.LENGTH_SHORT).show();
                        }
                        catch (NullPointerException e)
                        {
                            e.printStackTrace();
                        }
                        catch (NumberFormatException e)
                        {
                            e.printStackTrace();
                        }


                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(MeterReadingActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(MeterReadingActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }

    public void uploadMeterReading(View view) {
        AlertDialog.Builder alertDialog;
        //sign out individual from app
        alertDialog=new AlertDialog.Builder(MeterReadingActivity.this);
        alertDialog.setTitle("Upload Confirmation");
        alertDialog.setMessage("Are you sure you want to upload these details?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                acceptSendMeterDetails();

            }
        });
        alertDialog.setNegativeButton("NO", null);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void acceptSendMeterDetails(){
        url =ConstantValues.METER_READING_URL;
        getImageNameFromEditText = "meter";


        @SuppressLint("MissingPermission") Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);


        longitude =Double.toString(mLastLocation.getLongitude()) ;
        latitude = Double.toString(mLastLocation.getLatitude());
        //Toast.makeText(this, "latitude is " +latitude+  "\n"+ "Longitude is "+longitude, Toast.LENGTH_LONG).show();
        currentReadingText = currentReading.getText().toString();
        previousReadingText = previousReading.getText().toString();
        customerIdText = customerId;
        meterNumberText = meterNumber.getText().toString();
        unitsConsumedText = unitsConsumed.getText().toString();

        //code to ensure the drawable image in the imageView is not uploaded to the server
        //only the image captured by the camera should be uploaded

//        ImageView imageView = meterPicture;
//        if (R.id.meter_image == imageView.getId()) {
//            imageViewValue = null;
//        }

        if (!currentReadingText.isEmpty() && !previousReadingText.isEmpty() && !meterNumberText.isEmpty() && imageViewValue != null) {

            ImageUploadToServerFunction();
        } else {
            alertDialog = new AlertDialog.Builder(MeterReadingActivity.this);
            alertDialog.setMessage("Meter number, Previous reading or current reading must not be empty" + "\n" +
                    "You must also take a photo of the meter");
            alertDialog.setPositiveButton("OK", null);
            alertDialog.setCancelable(false);
            alertDialog.show();
        }

    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    //a method to get meter number when searched and populates views
    @SuppressLint("StaticFieldLeak")
    private class GetMeterNumber extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(MeterReadingActivity.this);
        HttpURLConnection conn;
        URL url2 = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... params) {
            try {

                url2 = new URL(ConstantValues.METER_NUMBER_SEARCH);

            } catch (MalformedURLException e) {

                e.printStackTrace();
                pdLoading.dismiss();
                return e.toString();
            }
            try {


                conn = (HttpURLConnection) url2.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput to true as we send and receive data
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // add parameter to our above url
                Uri.Builder builder = new Uri.Builder().appendQueryParameter("meterNumber", searchQuery);
                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e) {
                e.printStackTrace();
                pdLoading.dismiss();
                return e.toString();
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {
                    return ("Connection error");
                }

            } catch (IOException e) {
                e.printStackTrace();
                pdLoading.dismiss();
                return e.toString();
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String result) {

            pdLoading.dismiss();
            meterNumberDetails = new ArrayList<>();

            // pdLoading.dismiss();

            if (result.equals("[]")) {
                Toast.makeText(MeterReadingActivity.this, "No match found", Toast.LENGTH_LONG).show();
            } else {

                try {


                    JSONArray jArray = new JSONArray(result);

                    // Extract data from json and store into ArrayList as class objects
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject json_data = jArray.getJSONObject(i);
                        Search data = new Search();

                        meterNumber.setText(data.meterNumber = json_data.optString("meter_number"));
                        previousReading.setText(data.previousReading = json_data.optString("current_reading"));
                        customerId = (data.customerID = json_data.optString("cust_id"));


                        String numberOne = currentReading.getText().toString();
                        String numberTwo = previousReading.getText().toString();

                        if (!numberOne.isEmpty() && !numberTwo.isEmpty()) {
                            try {


                                int firstNumber = Integer.parseInt(numberOne);
                                int secondNumber = Integer.parseInt(numberTwo);

                                int unitsCalculation = firstNumber - secondNumber;
                                String calculationResult = Integer.toString(unitsCalculation);
                                unitsConsumed.setText(calculationResult);
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }

                    }


                } catch (JSONException e) {
                    Toast.makeText(MeterReadingActivity.this, "Connection error. Please make sure you have internet connection", Toast.LENGTH_LONG).show();

                }
            }

        }

    }


    //Image upload methods

    // Star activity for result method to Set captured image on image view after click.

//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == 7 && resultCode == RESULT_OK && data != null && data.getData() != null) {
//
//            Uri uri = data.getData();
//
//            try {
//
//                // Adding captured image in bitmap.
//                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//
//                // adding captured image in imageview.
//                imageView.setImageBitmap(bitmap);
//
//            } catch (IOException e) {
//
//                e.printStackTrace();
//            }
//        }
//
//    }


    // Upload captured image online on server function.
    public void ImageUploadToServerFunction() {

        ByteArrayOutputStream byteArrayOutputStreamObject;

        byteArrayOutputStreamObject = new ByteArrayOutputStream();

        // Converting bitmap image to jpeg format, so by default image will upload in jpeg format.
        try {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);
        } catch (NullPointerException e) {
            Toast.makeText(MeterReadingActivity.this, "No photo found", Toast.LENGTH_LONG).show();
        }

        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();

        final String convertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);

        @SuppressLint("StaticFieldLeak")
        class UploadMeterDetails extends AsyncTask<Void, Void, String> {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                // Showing progress dialog at image upload time.
                progressDialog = ProgressDialog.show(MeterReadingActivity.this, "Uploading Details", "Please Wait", false, false);
            }

            @Override
            protected void onPostExecute(String results) {

                super.onPostExecute(results);

                // Dismiss the progress dialog after done uploading.
                progressDialog.dismiss();

                // Printing uploading success message coming from server on android app.
                // Toast.makeText(ImageUpload.this,string1,Toast.LENGTH_LONG).show();


                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(MeterReadingActivity.this);
                alertDialog.setMessage(results);
                alertDialog.setPositiveButton("OK", null);
                alertDialog.setCancelable(false);
                alertDialog.show();

                setFieldstoNull();


                // Setting image as transparent after done uploading.
                // ImageViewHolder.setImageResource(android.R.color.transparent);


            }

            @Override
            protected String doInBackground(Void... params) {

                ImageProcessClass imageProcessClass = new ImageProcessClass();

                HashMap<String, String> HashMapParams = new HashMap<String, String>();
                HashMapParams.put(ImageNameFieldOnServer, getImageNameFromEditText);
                HashMapParams.put("current_reading", currentReadingText);
                HashMapParams.put("previous_reading", previousReadingText);
                HashMapParams.put("customerId", customerIdText);
                HashMapParams.put("meterNumber", meterNumberText);
                HashMapParams.put("unitsConsumed", unitsConsumedText);
                HashMapParams.put("latitude", latitude);
                HashMapParams.put("longitude", longitude);

                HashMapParams.put(ImagePathFieldOnServer, convertImage);

                return imageProcessClass.ImageHttpRequest(url, HashMapParams);
            }
        }
        UploadMeterDetails uploadMeterDetails = new UploadMeterDetails();

        uploadMeterDetails.execute();
    }

    public class ImageProcessClass {

        public String ImageHttpRequest(String requestURL, HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject;
                BufferedReader bufferedReaderObject;
                int RC;

                url = new URL(requestURL);

                httpURLConnectionObject = (HttpURLConnection) url.openConnection();

                httpURLConnectionObject.setReadTimeout(19000);

                httpURLConnectionObject.setConnectTimeout(19000);

                httpURLConnectionObject.setRequestMethod("POST");

                httpURLConnectionObject.setDoInput(true);

                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(

                        new OutputStreamWriter(OutPutStream, "UTF-8"));

                bufferedWriterObject.write(bufferedWriterDataFN(PData));

                bufferedWriterObject.flush();

                bufferedWriterObject.close();

                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));

                    stringBuilder = new StringBuilder();

                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null) {

                        stringBuilder.append(RC2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;

            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {

                if (check)

                    check = false;
                else
                    stringBuilderObject.append("&");

                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));

                stringBuilderObject.append("=");

                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }


    //Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        if (!isConnected) {
            String message = "No internet connection";
            int color = Color.CYAN;

            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.tv_forgot_password), message, Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }

    }

    //a method to get coordinates of meter
    @SuppressLint("StaticFieldLeak")
    private class GetLongitudeLatitude extends AsyncTask<String,Void,String> {

        HttpURLConnection conn;
        URL url2 = null;


        @Override
        protected String doInBackground(String... params) {
            try {

                url2 = new URL(ConstantValues.COORDINATES_URL);

            } catch (MalformedURLException e) {

                e.printStackTrace();
                return e.toString();
            }
            try {


                conn = (HttpURLConnection) url2.openConnection();
                conn.setReadTimeout(ConstantValues.READ_TIMEOUT);
                conn.setConnectTimeout(ConstantValues.CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput to true as we send and receive data
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // add parameter to our above url
                Uri.Builder builder = new Uri.Builder().appendQueryParameter("meterNumber", searchQuery);
                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {
                    return("Connection error");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String result) {


            try {


                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    Coordinates data = new Coordinates();

                    latitudeValue=(data.latitude = json_data.optString("latitude"));
                    longitudeValue=(data.longitude = json_data.optString("longitude"));
                    //System.out.println("Longitude is: "+longitudeValue);
                    //System.out.println("Latitude is: "+latitudeValue);


                }


            } catch (JSONException e) {
                Toast.makeText(MeterReadingActivity.this, "Connection error has occurred while getting meter coordinates. Make sure you have internet connection", Toast.LENGTH_LONG).show();

            }


        }

    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


    //implemented methods of KeyListener interface

    @Override
    public int getInputType() {
        return 0;
    }

    @Override
    public boolean onKeyDown(View view, Editable text, int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyUp(View view, Editable text, int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyOther(View view, Editable text, KeyEvent event) {
        return false;
    }

    @Override
    public void clearMetaKeyState(View view, Editable content, int states) {

    }

}
