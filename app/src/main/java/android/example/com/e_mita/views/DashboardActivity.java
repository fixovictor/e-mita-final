package android.example.com.e_mita.views;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.example.com.e_mita.R;
import android.example.com.e_mita.adapters.DashboardAdapter;
import android.example.com.e_mita.utils.ConstantValues;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class DashboardActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Integer> imageTitle = new ArrayList<>();
    private ArrayList<Integer> imageSrc = new ArrayList<>();
    private SharedPreferences mPreferences;
    private ImageView profileImageMenu;
    private int count=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        profileImageMenu = findViewById(R.id.profile_image_menu);
        profileImageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view);
            }
        });

        boolean isTablet = findViewById(R.id.dashboard_tablet) != null;
        boolean isSmall = findViewById(R.id.dashboard_small) != null;

        recyclerView = findViewById(R.id.rv_dashboard);
        recyclerView.setHasFixedSize(true);

        AutoFitGridLayoutManager layoutManager;

        if (isTablet) {
            layoutManager = new AutoFitGridLayoutManager(this, 400);
        } else if(isSmall) {
            layoutManager = new AutoFitGridLayoutManager(this, 250);
        }else{
            layoutManager = new AutoFitGridLayoutManager(this, 300);
        }

        recyclerView.setLayoutManager(layoutManager);
        populateViews();

        mPreferences = getSharedPreferences(ConstantValues.sharedPrefFile, MODE_PRIVATE);
        loadProfileImage();

    }

    public void loadProfileImage(){
        String profileUrl = mPreferences.getString(ConstantValues.PROFILE_IMAGE_URI, "");
        if(profileUrl.equals("")){
            profileImageMenu.setImageResource(R.drawable.ic_account_white);

        }else{
            Picasso.with(this).load(profileUrl)
                    .transform(new CropCircleTransformation())
                    .into((ImageView) findViewById(R.id.profile_image_menu));
        }
    }

    // Display anchored popup menu based on view selected
    private void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        // Inflate the menu from xml
        popup.inflate(R.menu.main_menu);
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_logout:
                        AlertDialog.Builder alertDialog;
                        //sign out individual from app
                        alertDialog=new AlertDialog.Builder(DashboardActivity.this);
                        alertDialog.setTitle("Logout Confirmation");
                        alertDialog.setMessage("Are you sure you want to logout?");
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent logout = new Intent(DashboardActivity.this, LoginActivity.class);
                                startActivity(logout);
                                finish();

                            }
                        });
                        alertDialog.setNegativeButton("NO", null);
                        alertDialog.setCancelable(false);
                        alertDialog.show();

                        return true;
                    case R.id.action_profile:
                        //Open profile activity
                        Intent profile = new Intent(DashboardActivity.this, ProfileActivity.class);
                        startActivity(profile);
                       // finish();
                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    private void populateViews() {
        imageTitle.add(R.string.meter_reading);
        imageTitle.add(R.string.tasks);
        imageTitle.add(R.string.schedule);
        imageTitle.add(R.string.complaints);
        imageTitle.add(R.string.statistics);
        imageTitle.add(R.string.location);

        imageSrc.add(R.drawable.ic_meter);
        imageSrc.add(R.drawable.ic_tasks);
        imageSrc.add(R.drawable.ic_schedule);
        imageSrc.add(R.drawable.ic_complaint);
        imageSrc.add(R.drawable.ic_statistics);
        imageSrc.add(R.drawable.ic_location);

        DashboardAdapter dashboardAdapter = new DashboardAdapter(DashboardActivity.this, imageTitle, imageSrc);
        recyclerView.setAdapter(dashboardAdapter);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed()
    {

        for(int i=0;i<=1;i++)
        if(count == 1)
        {
            count=0;
            //method to close application
            finishAffinity();
        }
        else
        {
            Toast.makeText(DashboardActivity.this, "Press Back again to exit.", Toast.LENGTH_SHORT).show();
            count++;
        }
    }
}
