package android.example.com.e_mita.views;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.example.com.e_mita.BuildConfig;
import android.example.com.e_mita.R;
import android.example.com.e_mita.models.Profile;
import android.example.com.e_mita.utils.ConstantValues;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.method.KeyListener;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

import static android.example.com.e_mita.utils.ConstantValues.REQUEST_IMAGE;

public class ComplaintsActivity extends AppCompatActivity implements KeyListener {

    private Spinner complaintSpinner;
    private ImageView complaintImage, profileImageMenu;
    private FusedLocationProviderClient client;
    private EditText userName, userEmail, complaintMessage;
    private String emailText, typeOfComplaintText, nameText, message;
    private AlertDialog.Builder alertDialog;
    private ProgressDialog progressDialog;
    private SharedPreferences mPreferences;
    private String imageViewValue;

    //by Fixo

    Intent intent;
    Bitmap bitmap;
    String getImageNameFromEditText;
    String url, searchQuery, latitude, longitude, compressedImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaints);

        Toolbar toolbar = findViewById(R.id.toolbar_complaints);
        setSupportActionBar(toolbar);

        //initializing imageView to null to ensure a photo is only uploaded when the camera has taken a photo
        imageViewValue = null;

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        profileImageMenu = findViewById(R.id.profile_image_menu_complaints);
        profileImageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view);
            }
        });

        mPreferences = getSharedPreferences(ConstantValues.sharedPrefFile, MODE_PRIVATE);
        loadProfileImage();

        complaintSpinner = findViewById(R.id.complaints_spinner);
        complaintImage = findViewById(R.id.complaint_image);
        userName = findViewById(R.id.ev_username);
        userEmail = findViewById(R.id.ev_email_address);
        complaintMessage = findViewById(R.id.ev_message);

        client = LocationServices.getFusedLocationProviderClient(this);


        //getting email address of logged on user to retrieve Technician username
        SharedPreferences sharedPreferences = getSharedPreferences("Login_Email", Context.MODE_PRIVATE);
        searchQuery = sharedPreferences.getString("email", "");
        userEmail.setText(searchQuery);

        GettingTechnicianName gettingTechnicianName = new GettingTechnicianName();
        gettingTechnicianName.execute(searchQuery);

        //making username and email uneditable
        userEmail.setKeyListener(null);
        userName.setKeyListener(null);

        FloatingActionButton fabSend = findViewById(R.id.fab_send);
        fabSend.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View view) {
                //Compressing image and storing it in compressedImage string
                ByteArrayOutputStream byteArrayOutputStreamObject;

                byteArrayOutputStreamObject = new ByteArrayOutputStream();

                // Converting bitmap image to jpeg format, so by default image will upload in jpeg format.
                try {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    // Toast.makeText(ComplaintsActivity.this, "No photo found", Toast.LENGTH_LONG).show();
                }

                byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();

                compressedImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);//getting the longitude and latitude of the current position of the technician

                url = ConstantValues.COMPLAINTS_URL;
                getImageNameFromEditText = "meter";
                emailText = userEmail.getText().toString();
                nameText = userName.getText().toString();
                message = complaintMessage.getText().toString();
                typeOfComplaintText = complaintSpinner.getSelectedItem().toString();

                if (!emailText.isEmpty() && !nameText.isEmpty() && !message.isEmpty() && !typeOfComplaintText.isEmpty() && imageViewValue != null) {
                    //imageView.getDrawable()!=null
                    final UploadComplaintDetailsWithPhoto uploadComplaintDetails = new UploadComplaintDetailsWithPhoto();
                    client.getLastLocation().addOnSuccessListener(ComplaintsActivity.this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {

                                latitude = Double.toString(location.getLatitude());
                                longitude = Double.toString(location.getLongitude());
                                // Toast.makeText(getApplicationContext(), "Latitude" + location.getLatitude() + "\n" + "Longitude" + location.getLongitude(), Toast.LENGTH_LONG).show();
                                uploadComplaintDetails.execute(getImageNameFromEditText, emailText, nameText, message, typeOfComplaintText, longitude, latitude, compressedImage);
                            }
                        }
                    });


                }
                else if (!emailText.isEmpty() && !nameText.isEmpty() && !message.isEmpty() && !typeOfComplaintText.isEmpty() && imageViewValue == null) {

                    final UploadComplaintDetailsWithoutPhoto uploadComplaintDetails = new UploadComplaintDetailsWithoutPhoto();
                    //getting the longitude and latitude of the current position of the technician
                    client.getLastLocation().addOnSuccessListener(ComplaintsActivity.this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {

                                latitude = Double.toString(location.getLatitude());
                                longitude = Double.toString(location.getLongitude());
                                uploadComplaintDetails.execute(emailText, nameText, message, typeOfComplaintText, longitude, latitude);
                                // Toast.makeText(getApplicationContext(), "Latitude" + location.getLatitude() + "\n" + "Longitude" + location.getLongitude(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } else {
                    alertDialog = new AlertDialog.Builder(ComplaintsActivity.this);
                    alertDialog.setMessage("Message should not be empty!");
                    alertDialog.setPositiveButton("OK", null);
                    alertDialog.setCancelable(false);
                    alertDialog.show();
                }
            }
        });

        FloatingActionButton fabCamera = findViewById(R.id.fab_camera);
        fabCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureImage();
            }
        });

        ArrayAdapter<CharSequence> adapterComplaints = ArrayAdapter.createFromResource(this, R.array.complaints, android.R.layout.simple_spinner_item);

        //Specify layout for each dropdown list for spinner
        adapterComplaints.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Add adapter to spinner
        complaintSpinner.setAdapter(adapterComplaints);
    }

    public void loadProfileImage() {
        String profileUrl = mPreferences.getString(ConstantValues.PROFILE_IMAGE_URI, "");
        if (profileUrl.equals("")) {
            profileImageMenu.setImageResource(R.drawable.ic_account_white);

        } else {
            Picasso.with(this).load(profileUrl)
                    .transform(new CropCircleTransformation())
                    .into(profileImageMenu);
        }
    }

    // Display anchored popup menu based on view selected
    private void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        // Inflate the menu from xml
        popup.inflate(R.menu.main_menu);
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_logout:
                        //sign out individual from app
                        alertDialog = new AlertDialog.Builder(ComplaintsActivity.this);
                        alertDialog.setTitle("Logout Confirmation");
                        alertDialog.setMessage("Are you sure you want to logout?");
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent logout = new Intent(ComplaintsActivity.this, LoginActivity.class);
                                startActivity(logout);
                                finish();

                            }
                        });
                        alertDialog.setNegativeButton("NO", null);
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                        return true;
                    case R.id.action_profile:
                        //Open profile activity
                        Intent profile = new Intent(ComplaintsActivity.this, ProfileActivity.class);
                        startActivity(profile);

                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ComplaintsActivity.this, DashboardActivity.class);
        startActivity(intent);
    }

    public void attachPhoto(View view) {
        captureImage();
    }

    private void captureImage() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, ConstantValues.REQUEST_IMAGE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK) {
            // if the result is capturing Image
            //Fill the imageview with the image
            bitmap = (Bitmap) data.getExtras().get("data");
            complaintImage.setImageBitmap(bitmap);
            complaintImage.setVisibility(View.VISIBLE);
            imageViewValue = "1";

            getGPScoordinates();
        }
    }


    private void getGPScoordinates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Dexter.withActivity(this)
                    .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse response) {
                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse response) {
                            if (response.isPermanentlyDenied()) {
                                //open device settings when the permission is denied permanently
                                openSettings();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
        }

        client.getLastLocation().addOnSuccessListener(ComplaintsActivity.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    //Toast.makeText(getApplicationContext(), "Latitude" + location.getLatitude() + "\n" + "Longitude" + location.getLongitude(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    /**
     * the two methods (onDestroy() and onPause() ) help prevent this error
     * * leaked window com.android.internal.policy.PhoneWindow$DecorView{8feb48a V.E...... R......D 0,0-668,322} that was originally added here
     **/
    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            progressDialog.dismiss();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            progressDialog.dismiss();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class UploadComplaintDetailsWithPhoto extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... voids) {
            try {
                URL url = new URL(ConstantValues.COMPLAINTS_URL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setReadTimeout(ConstantValues.READ_TIMEOUT);
                httpURLConnection.setConnectTimeout(ConstantValues.CONNECTION_TIMEOUT);

                //opening an output stream
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String data_to_post = URLEncoder.encode("image_name", "UTF-8") + "=" + URLEncoder.encode(getImageNameFromEditText, "UTF-8") + "&"
                        + URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(emailText, "UTF-8") + "&"
                        + URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(nameText, "UTF-8") + "&"
                        + URLEncoder.encode("message", "UTF-8") + "=" + URLEncoder.encode(message, "UTF-8") + "&"
                        + URLEncoder.encode("complaint", "UTF-8") + "=" + URLEncoder.encode(typeOfComplaintText, "UTF-8") + "&"
                        + URLEncoder.encode("longitude", "UTF-8") + "=" + URLEncoder.encode(longitude, "UTF-8") + "&"
                        + URLEncoder.encode("latitude", "UTF-8") + "=" + URLEncoder.encode(latitude, "UTF-8") + "&"
                        + URLEncoder.encode("image_path", "UTF-8") + "=" + URLEncoder.encode(compressedImage, "UTF-8");
                bufferedWriter.write(data_to_post);

                //closing open objects
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                Log.i("Data", "Message" + data_to_post);

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String line;
                StringBuilder result = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }

                //closing open connections
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            } catch (MalformedURLException e) {
                progressDialog.dismiss();
                e.printStackTrace();
            } catch (IOException e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            alertDialog = new AlertDialog.Builder(ComplaintsActivity.this);
            alertDialog.setTitle("Response");
            progressDialog = ProgressDialog.show(ComplaintsActivity.this, "Uploading your complaint", "Please Wait", false, false);
        }

        @Override
        protected void onPostExecute(String results) {
            // Dismiss the progress dialog after done uploading.
            progressDialog.dismiss();

            // Printing uploading success message coming from server on android app.
            // Toast.makeText(ImageUpload.this,string1,Toast.LENGTH_LONG).show();

            try {
                if (results != null) {

                    if (results.contains("We have received your complaint. Thank you")) {
                        alertDialog.setMessage(results);
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(ComplaintsActivity.this, DashboardActivity.class);
                                startActivity(intent);
                            }
                        });
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                    } else {
                        alertDialog.setMessage(results);
                        alertDialog.setPositiveButton("OK", null);
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                    }


                } else {

                    Toast.makeText(ComplaintsActivity.this, "Connection error. Please ensure you have internet connection", Toast.LENGTH_LONG).show();
                }
            } catch (NullPointerException e) {
                Toast.makeText(ComplaintsActivity.this, "Connection error. Please ensure you have internet connection", Toast.LENGTH_LONG).show();

            }


        }

    }


    @SuppressLint("StaticFieldLeak")
    private class UploadComplaintDetailsWithoutPhoto extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... voids) {
            try {
                URL url = new URL(ConstantValues.COMPLAINTS_URL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setReadTimeout(ConstantValues.READ_TIMEOUT);
                httpURLConnection.setConnectTimeout(ConstantValues.CONNECTION_TIMEOUT);

                //opening an output stream
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String data_to_post = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(emailText, "UTF-8") + "&"
                        + URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(nameText, "UTF-8") + "&"
                        + URLEncoder.encode("message", "UTF-8") + "=" + URLEncoder.encode(message, "UTF-8") + "&"
                        + URLEncoder.encode("complaint", "UTF-8") + "=" + URLEncoder.encode(typeOfComplaintText, "UTF-8") + "&"
                        + URLEncoder.encode("longitude", "UTF-8") + "=" + URLEncoder.encode(longitude, "UTF-8") + "&"
                        + URLEncoder.encode("latitude", "UTF-8") + "=" + URLEncoder.encode(latitude, "UTF-8");

                bufferedWriter.write(data_to_post);

                //closing open objects
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                Log.i("Data", "Message" + data_to_post);

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String line;
                StringBuilder result = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }

                //closing open connections
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            } catch (MalformedURLException e) {
                progressDialog.dismiss();
                e.printStackTrace();
            } catch (IOException e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            alertDialog = new AlertDialog.Builder(ComplaintsActivity.this);
            alertDialog.setTitle("Response");
            progressDialog = ProgressDialog.show(ComplaintsActivity.this, "Uploading your complaint", "Please Wait", false, false);
        }

        @Override
        protected void onPostExecute(String results) {
            // Dismiss the progress dialog after done uploading.
            progressDialog.dismiss();

            // Printing uploading success message coming from server on android app.
            // Toast.makeText(ImageUpload.this,string1,Toast.LENGTH_LONG).show();

            try {
                if (results != null) {

                    if (results.contains("We have received your complaint. Thank you")) {
                        alertDialog.setMessage(results);
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(ComplaintsActivity.this, DashboardActivity.class);
                                startActivity(intent);
                            }
                        });
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                    } else {
                        alertDialog.setMessage(results);
                        alertDialog.setPositiveButton("OK", null);
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                    }


                } else {

                    Toast.makeText(ComplaintsActivity.this, "Connection error. Please ensure you have internet connection", Toast.LENGTH_LONG).show();
                }
            } catch (NullPointerException e) {
                Toast.makeText(ComplaintsActivity.this, "Connection error. Please ensure you have internet connection", Toast.LENGTH_LONG).show();

            }


        }

    }

    //implemented methods of KeyListener
    @Override
    public int getInputType() {
        return 0;
    }

    @Override
    public boolean onKeyDown(View view, Editable text, int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyUp(View view, Editable text, int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyOther(View view, Editable text, KeyEvent event) {
        return false;
    }

    @Override
    public void clearMetaKeyState(View view, Editable content, int states) {

    }


    @SuppressLint("StaticFieldLeak")
    private class GettingTechnicianName extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... voids) {
            try {
                URL url = new URL(ConstantValues.TECHNICIAN_USERNAME_URL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setReadTimeout(ConstantValues.READ_TIMEOUT);
                httpURLConnection.setConnectTimeout(ConstantValues.CONNECTION_TIMEOUT);

                //opening an output stream
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String data_to_post = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(searchQuery, "UTF-8");
                bufferedWriter.write(data_to_post);

                //closing open objects
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String line;
                StringBuilder result = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }

                //closing open connections
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            } catch (MalformedURLException e) {

                e.printStackTrace();
            } catch (IOException e) {

                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {

            try {


                JSONArray jArray = new JSONArray(result);

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    Profile data = new Profile();
                    userName.setText(data.userName = json_data.optString("Tech_username"));
                }


            } catch (JSONException e) {
                Toast.makeText(ComplaintsActivity.this, "An error has occurred while loading your name. Please ensure you have internet connection", Toast.LENGTH_LONG).show();

            } catch (NullPointerException a) {
                Toast.makeText(ComplaintsActivity.this, "An error has occurred while loading your name. Please ensure you have internet connection", Toast.LENGTH_LONG).show();

            }

        }

    }


}




