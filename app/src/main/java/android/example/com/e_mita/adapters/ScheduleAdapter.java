package android.example.com.e_mita.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.example.com.e_mita.R;
import android.example.com.e_mita.models.Schedule;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> {

    private List<Schedule> listItems;
    private Context context;

    public ScheduleAdapter(Context mContext, List<Schedule> listItems) {
        this.listItems = listItems;
        this.context = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Schedule itemList = listItems.get(position);

        //Formatting and displaying timestamp
        holder.scheduleTitle.setText(formatDate(itemList.getTitle()));

        holder.scheduleDescription.setText(itemList.getDescription());

        //Displaying dot from HTML character code
        holder.dot.setText(Html.fromHtml("&#8226;"));
    }

    /*
     * Formatting timestamp to `MMM d` format
     * Input: 2018-02-21 00:15:42
     * Output: Feb 21
     */

    private String formatDate(String string) {
        try{
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = simpleDateFormat.parse(string);
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleFormat = new SimpleDateFormat("dd/MM/yy");
            return simpleFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView scheduleTitle;
        TextView scheduleDescription;
        TextView dot;

        ViewHolder(View itemView) {
            super(itemView);
            scheduleTitle = itemView.findViewById(R.id.schedule_title);
            scheduleDescription = itemView.findViewById(R.id.scheduleDescription);
            dot = itemView.findViewById(R.id.dot);
        }
    }
}
