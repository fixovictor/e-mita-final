package android.example.com.e_mita.adapters;

import android.content.Context;
import android.content.Intent;
import android.example.com.e_mita.R;
import android.example.com.e_mita.models.Task;
import android.example.com.e_mita.views.TaskStatusActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {

    private Context context;
    private List<Task> taskDetails;

    //constructor method
    public TaskAdapter(Context context, List<Task> taskDetails) {
        this.context = context;
        this.taskDetails = taskDetails;
    }

    @NonNull
    @Override
    public TaskAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //setting the layout to be displayed when oncCreateViewHolder method is called
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_list_item, parent, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull TaskAdapter.ViewHolder holder, int position) {
        final Task task = taskDetails.get(position);
        holder.taskDescription.setText(task.getTaskName());
        String status = task.getCompleteStatus();

        if (status.contains("1")) {
            holder.completeTask.setVisibility(View.VISIBLE);
        } else {
            holder.incompleteTask.setVisibility(View.VISIBLE);
        }


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String taskStatusValue = task.getCompleteStatus();
                String taskDescription = task.getTaskName();
                Intent intent = new Intent(context, TaskStatusActivity.class);
                intent.putExtra("description", taskDescription);
                intent.putExtra("taskStatus", taskStatusValue);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return taskDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView taskDescription;

        public RelativeLayout cardView;
        public ImageView completeTask, incompleteTask;

        public ViewHolder(View itemView) {
            super(itemView);
            taskDescription = itemView.findViewById(R.id.task_description);

            cardView = itemView.findViewById(R.id.parent_layout);
            completeTask = itemView.findViewById(R.id.completeStatus);
            incompleteTask = itemView.findViewById(R.id.incompleteStatus);
        }
    }


}
