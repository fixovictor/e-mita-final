package android.example.com.e_mita.models;

public class Customer {
    private String customerName,longitude,latitude;

    public Customer(String customerName) {
        this.customerName = customerName;
    }

    public Customer(String longitude, String latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}
