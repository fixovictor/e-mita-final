package android.example.com.e_mita.views;


import android.content.Context;
import android.content.SharedPreferences;
import android.example.com.e_mita.R;
import android.example.com.e_mita.utils.ConstantValues;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragmentCompat {

    private SharedPreferences notificationPreferences;
    private String sharedNotPrefFile = "com.example.android.emita.notification";

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);

        notificationPreferences = this.getActivity().getSharedPreferences(sharedNotPrefFile, Context.MODE_PRIVATE);
        Preference preference = this.findPreference(ConstantValues.KEY_PREF_NOTIFICATION_SWITCH);
        preference.setSummary(notificationPreferences.getString("summary", getString(R.string.notification_off)));
        preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if((Boolean) newValue == true){
                    preference.setSummary(R.string.notification_on);
                    SharedPreferences.Editor preferencesEditor = notificationPreferences.edit();
                    preferencesEditor.putString("summary", getString(R.string.notification_on)).apply();
                }
                else{
                    preference.setSummary(R.string.notification_off);
                    SharedPreferences.Editor preferencesEditor = notificationPreferences.edit();
                    preferencesEditor.putString("summary", getString(R.string.notification_off)).apply();
                }
                return true;
            }
        });
    }

}
