package android.example.com.e_mita.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.example.com.e_mita.R;
import android.example.com.e_mita.adapters.TaskAdapter;
import android.example.com.e_mita.models.Task;
import android.example.com.e_mita.utils.ConstantValues;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class TasksActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TextView dateView, monthView;
    private ImageView profileImageMenu;
    private SharedPreferences mPreferences;
    private String email;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private ProgressDialog progressDialog;
    private AlertDialog.Builder alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);

        Toolbar toolbar = findViewById(R.id.toolbar_tasks);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        //getting email address of logged on user to retrieve tasks assigned to them
        SharedPreferences sharedPreferences = getSharedPreferences("Login_Email", Context.MODE_PRIVATE);
        email = sharedPreferences.getString("email", "");

        dateView = findViewById(R.id.tv_date_number);
        monthView = findViewById(R.id.tv_month);

        recyclerView = findViewById(R.id.rv_tasks);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        //executing the background class to send data to the database and get results
        GetTasks getTasks = new GetTasks();
        getTasks.execute(email);

        populateViews();

        profileImageMenu = findViewById(R.id.profile_image_menu_tasks);
        profileImageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view);
            }
        });

        mPreferences = getSharedPreferences(ConstantValues.sharedPrefFile, MODE_PRIVATE);
        loadProfileImage();

        ImageView refreshButton = findViewById(R.id.refresh_tasks);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetTasks getTasks = new GetTasks();
                getTasks.execute(email);
            }
        });

    }

    public void loadProfileImage() {
        String profileUrl = mPreferences.getString(ConstantValues.PROFILE_IMAGE_URI, "");
        if (profileUrl.equals("")) {
            profileImageMenu.setImageResource(R.drawable.ic_account_white);

        } else {
            Picasso.with(this).load(profileUrl)
                    .transform(new CropCircleTransformation())
                    .into(profileImageMenu);
        }
    }

    // Display anchored popup menu based on view selected
    private void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        // Inflate the menu from xml
        popup.inflate(R.menu.main_menu);
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_logout:
                        //sign out individual from app
                        alertDialog=new AlertDialog.Builder(TasksActivity.this);
                        alertDialog.setTitle("Logout Confirmation");
                        alertDialog.setMessage("Are you sure you want to logout?");
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent logout = new Intent(TasksActivity.this, LoginActivity.class);
                                startActivity(logout);
                                finish();

                            }
                        });
                        alertDialog.setNegativeButton("NO", null);
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                        return true;
                    case R.id.action_profile:
                        //Open profile activity
                        Intent profile = new Intent(TasksActivity.this, ProfileActivity.class);
                        startActivity(profile);

                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }


    private void populateViews() {
        Calendar c = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat("dd");
        String formattedDate = df.format(c.getTime());
        dateView.setText(formattedDate);

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat mf = new SimpleDateFormat("MMM");
        String formattedMonth = mf.format(c.getTime());
        monthView.setText(formattedMonth);

    }

    /** the two methods (onDestroy() and onPause() ) help prevent this error
     ** leaked window com.android.internal.policy.PhoneWindow$DecorView{8feb48a V.E...... R......D 0,0-668,322} that was originally added here
     **/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    protected void onPause() {
        super.onPause();
        progressDialog.dismiss();
    }

    //a class to retrieve tasks of logged on user
    @SuppressLint("StaticFieldLeak")
    private class GetTasks extends AsyncTask<String, Void, String> {


        HttpURLConnection conn;
        URL url = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(TasksActivity.this);
            progressDialog.setMessage("\tLoading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            try {

                url = new URL(ConstantValues.TASKS_URL);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Toast.makeText(TasksActivity.this, "Connection error, please try again", Toast.LENGTH_LONG).show();
                return e.toString();
            }
            try {

                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput to true as we send and recieve data
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // add parameter to our above url
                Uri.Builder builder = new Uri.Builder().appendQueryParameter("email", email);
                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e) {
                try {
                    Toast.makeText(TasksActivity.this, "Connection error, please try again", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                catch (RuntimeException a)
                {
                    a.printStackTrace();
                    //Toast.makeText(TasksActivity.this, "Connection error, please try again", Toast.LENGTH_LONG).show();
                }
                return e.toString();
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {
                    return ("Connection error");
                }

            } catch (IOException e) {
                e.printStackTrace();
                try{
                    Toast.makeText(TasksActivity.this, "Connection error. Please ensure you have internet connection", Toast.LENGTH_LONG).show();
                }catch (RuntimeException a){
                    a.printStackTrace();
                }

                return e.toString();
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String result) {

            progressDialog.dismiss();
            List<Task> taskDetailsList = new ArrayList<>();

            progressDialog.dismiss();

            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    Task task = new Task(json_data.getString("task_name"), json_data.getString("completed"));

                    taskDetailsList.add(task);
                }

                // Setup and Handover data to recyclerview
                recyclerView = findViewById(R.id.rv_tasks);
                RecyclerView.Adapter adapter = new TaskAdapter(TasksActivity.this, taskDetailsList);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(TasksActivity.this));

            } catch (JSONException e) {
                Toast.makeText(TasksActivity.this, "An error has occurred while loading tasks. Please ensure you have internet connection then" +
                        " refresh", Toast.LENGTH_LONG).show();

            }

        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TasksActivity.this, DashboardActivity.class);
        startActivity(intent);
    }
}
