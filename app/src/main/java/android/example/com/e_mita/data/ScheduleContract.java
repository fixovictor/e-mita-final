package android.example.com.e_mita.data;

import android.provider.BaseColumns;

public class ScheduleContract {
    public static final class ScheduleEntry implements BaseColumns {
        //Favorite table and column names
        public static final String TABLE_NAME = "schedule";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_DESCRIPTION = "description";
    }
}

