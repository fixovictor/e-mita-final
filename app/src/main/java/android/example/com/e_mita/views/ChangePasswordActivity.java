package android.example.com.e_mita.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.example.com.e_mita.R;
import android.example.com.e_mita.utils.ConstantValues;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class ChangePasswordActivity extends AppCompatActivity {

    private ImageView hideCurrentPass, showCurrentPass, hideNewPass, showNewPass;
    private EditText currentPass, newPass;
    private String currentPassText,newPassText;
    private AlertDialog.Builder alertDialog;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Toolbar toolbar = findViewById(R.id.toolbar_change_password);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        currentPass = findViewById(R.id.ev_current_password);
        newPass = findViewById(R.id.ev_new_password);
        hideCurrentPass = findViewById(R.id.hide_current_password);
        showCurrentPass = findViewById(R.id.show_current_password);
        hideNewPass = findViewById(R.id.hide_new_password);
        showNewPass = findViewById(R.id.show_new_password);
    }

    public void updateUserPassword(View view) {
        newPassText=newPass.getText().toString();
        currentPassText=currentPass.getText().toString();

        if (!newPassText.isEmpty() && !currentPassText.isEmpty())
        {
            UploadNewChanges uploadNewChanges=new UploadNewChanges();
            uploadNewChanges.execute(currentPassText,newPassText);
        }
        else
        {
            alertDialog=new AlertDialog.Builder(ChangePasswordActivity.this);
            alertDialog.setMessage("Do not leave any field empty");
            alertDialog.setPositiveButton("OK",null);
            alertDialog.setCancelable(false);
            alertDialog.show();
        }

    }

    public void openForgotActivity(View view) {
        Intent forgotPassword = new Intent (ChangePasswordActivity.this, ForgotPasswordActivity.class);
        startActivity(forgotPassword);
    }

    public void revealCurrentPasswordCharacters(View view) {
        hideCurrentPass.setVisibility(View.INVISIBLE);
        showCurrentPass.setVisibility(View.VISIBLE);

        currentPass.setTransformationMethod(null);
    }

    public void hideCurrentPasswordCharacters(View view) {
        showCurrentPass.setVisibility(View.INVISIBLE);
        hideCurrentPass.setVisibility(View.VISIBLE);

        currentPass.setTransformationMethod(new PasswordTransformationMethod());
    }

    public void revealNewPasswordCharacters(View view) {
        hideNewPass.setVisibility(View.INVISIBLE);
        showNewPass.setVisibility(View.VISIBLE);

        newPass.setTransformationMethod(null);
    }

    public void hideNewPasswordCharacters(View view) {
        showNewPass.setVisibility(View.INVISIBLE);
        hideNewPass.setVisibility(View.VISIBLE);

        newPass.setTransformationMethod(new PasswordTransformationMethod());
    }
    //class to upload changed password
    @SuppressLint("StaticFieldLeak")
    private class UploadNewChanges extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... voids) {
            try {
                URL url=new URL(ConstantValues.NEW_PASSWORD_URL);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setReadTimeout(ConstantValues.READ_TIMEOUT);
                httpURLConnection.setConnectTimeout(ConstantValues.CONNECTION_TIMEOUT);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                //opening an output stream
                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data_to_post= URLEncoder.encode("old_password","UTF-8")+"="+URLEncoder.encode(currentPassText,"UTF-8")+"&"
                        + URLEncoder.encode("new_password","UTF-8")+"="+URLEncoder.encode(newPassText,"UTF-8");
                bufferedWriter.write(data_to_post);

                //closing open objects
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String line;
                StringBuilder result= new StringBuilder();

                while((line=bufferedReader.readLine())!=null)
                {
                    result.append(line);
                }

                //closing open connections
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            } catch (MalformedURLException e) {
                progressDialog.dismiss();
                e.printStackTrace();
            } catch (IOException e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            alertDialog=new AlertDialog.Builder(ChangePasswordActivity.this);
            alertDialog.setTitle("Change Status");
            progressDialog = ProgressDialog.show(ChangePasswordActivity.this,"Uploading changes","Please Wait",false,false);
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();

            try {

                if (result.contains("Your password has been changed successfully")) {
                    alertDialog.setMessage(result);
                    alertDialog.setCancelable(false);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(ChangePasswordActivity.this, ProfileActivity.class);
                            startActivity(intent);
                        }
                    });
                    alertDialog.show();
                } else {
                    alertDialog.setMessage(result);
                    alertDialog.setCancelable(false);
                    alertDialog.show();
                }
            }
            catch (NullPointerException e)
            {
                Toast.makeText(ChangePasswordActivity.this,"An error occurred. Please ensure you have internt connection",Toast.LENGTH_LONG).show();
            }

        }
    }

}
