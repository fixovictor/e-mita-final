package android.example.com.e_mita.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.example.com.e_mita.data.ScheduleContract.ScheduleEntry;
import android.example.com.e_mita.models.Schedule;

import java.util.ArrayList;
import java.util.List;

public class ScheduleDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "notes.db";
    private static final int DATABASE_VERSION = 1;

    //Create a helper object for the Journal database
    public ScheduleDbHelper(Context con){
        super(con, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String CREATE_TABLE = "CREATE TABLE " + ScheduleEntry.TABLE_NAME + " (" +
                ScheduleEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ScheduleEntry.COLUMN_TITLE + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                ScheduleEntry.COLUMN_DESCRIPTION + " TEXT NOT NULL);";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ScheduleEntry.TABLE_NAME);
        onCreate(db);
    }

    public long insertSchedule(String schedule){
        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //id and timestamp(title) will be inserted automatically
        values.put(ScheduleEntry.COLUMN_DESCRIPTION, schedule);

        //insert row
        long id = db.insert(ScheduleEntry.TABLE_NAME, null, values);

        //close db connection
        db.close();

        //return newly inserted row id
        return id;
    }

    public Schedule getSchedule(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(ScheduleEntry.TABLE_NAME,
                new String[]{ScheduleEntry.COLUMN_ID, ScheduleEntry.COLUMN_TITLE, ScheduleEntry.COLUMN_DESCRIPTION},
                ScheduleEntry.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        Schedule note = new Schedule(
                cursor.getInt(cursor.getColumnIndex(ScheduleEntry.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(ScheduleEntry.COLUMN_TITLE)),
                cursor.getString(cursor.getColumnIndex(ScheduleEntry.COLUMN_DESCRIPTION)));

        // close the db connection
        cursor.close();

        return note;
    }

    public List<Schedule> getAllNotes() {
        List<Schedule> schedule = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + ScheduleEntry.TABLE_NAME + " ORDER BY " +
                ScheduleEntry.COLUMN_TITLE + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Schedule note = new Schedule();
                note.setId(cursor.getInt(cursor.getColumnIndex(ScheduleEntry.COLUMN_ID)));
                note.setTitle(cursor.getString(cursor.getColumnIndex(ScheduleEntry.COLUMN_TITLE)));
                note.setDescription(cursor.getString(cursor.getColumnIndex(ScheduleEntry.COLUMN_DESCRIPTION)));

                schedule.add(note);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return schedule;
    }

    public int getNotesCount() {
        String countQuery = "SELECT  * FROM " + ScheduleEntry.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public int updateNote(Schedule schedule){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ScheduleEntry.COLUMN_DESCRIPTION, schedule.getDescription());
        //updating row
        return db.update(ScheduleEntry.TABLE_NAME, values, ScheduleEntry.COLUMN_ID + " = ?",
                new String[]{String.valueOf(schedule.getId())});
    }

    public void deleteNote(Schedule schedule){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ScheduleEntry.TABLE_NAME, ScheduleEntry.COLUMN_ID + " = ?",
                new String[]{String.valueOf(schedule.getId())});
        db.close();
    }
}

