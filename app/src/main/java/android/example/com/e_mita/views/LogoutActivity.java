package android.example.com.e_mita.views;

import android.content.DialogInterface;
import android.content.Intent;
import android.example.com.e_mita.R;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by FIXO on 12/01/2019.
 */

public class LogoutActivity extends AppCompatActivity {
    private AlertDialog.Builder alertDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        logUserOut();
    }

    public void logUserOut()
    {
        alertDialog=new AlertDialog.Builder(LogoutActivity.this);
        alertDialog.setTitle("Logout Confirmation");
        alertDialog.setMessage("Are you sure you want to logout?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent logout = new Intent(LogoutActivity.this, LoginActivity.class);
                startActivity(logout);
                finish();

            }
        });
        alertDialog.setNegativeButton("NO", null);
        alertDialog.setCancelable(false);
        alertDialog.show();

    }
}
