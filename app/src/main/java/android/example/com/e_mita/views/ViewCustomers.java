package android.example.com.e_mita.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.example.com.e_mita.R;
import android.example.com.e_mita.adapters.CustomerAdapter;
import android.example.com.e_mita.models.Customer;
import android.example.com.e_mita.utils.ConstantValues;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class ViewCustomers extends AppCompatActivity {

      private RecyclerView recyclerView;
//    private CustomerAdapter customerAdapter;
//    private ArrayList<Customer> customerNames = new ArrayList<>();


    private List<Customer> customerNames;
    private RecyclerView.Adapter customerAdapter;
    private String locationName;
    private ImageView profileImageMenu;
    private SharedPreferences mPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_customers);

        Toolbar toolbar = findViewById(R.id.toolbar_view_customers);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        profileImageMenu = findViewById(R.id.profile_image_menu_view_customers);
        profileImageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view);
            }
        });

        mPreferences = getSharedPreferences(ConstantValues.sharedPrefFile, MODE_PRIVATE);
        loadProfileImage();

        locationName=getIntent().getStringExtra("locationName");

        //executing the background class to send data to the database and get results
        GetCustomerNames getNames=new GetCustomerNames();
        getNames.execute(locationName);
    }

    public void loadProfileImage(){
        String profileUrl = mPreferences.getString(ConstantValues.PROFILE_IMAGE_URI, "");
        if(profileUrl.equals("")){
            profileImageMenu.setImageResource(R.drawable.ic_account_white);

        }else{
            Picasso.with(this).load(profileUrl)
                    .transform(new CropCircleTransformation())
                    .into(profileImageMenu);
        }
    }

    // Display anchored popup menu based on view selected
    private void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        // Inflate the menu from xml
        popup.inflate(R.menu.main_menu);
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_logout:
                        //sign out individual from app
                        AlertDialog.Builder alertDialog;
                        //sign out individual from app
                        alertDialog=new AlertDialog.Builder(ViewCustomers.this);
                        alertDialog.setTitle("Logout Confirmation");
                        alertDialog.setMessage("Are you sure you want to logout?");
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent logout = new Intent(ViewCustomers.this, LoginActivity.class);
                                startActivity(logout);
                                finish();

                            }
                        });
                        alertDialog.setNegativeButton("NO", null);
                        alertDialog.setCancelable(false);
                        alertDialog.show();

                        return true;
                    case R.id.action_profile:
                        //Open profile activity
                        Intent profile = new Intent(ViewCustomers.this, ProfileActivity.class);
                        startActivity(profile);
                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ViewCustomers.this, LocationActivity.class);
        startActivity(intent);
    }

    // Create class AsyncFetch
    @SuppressLint("StaticFieldLeak")
    private class GetCustomerNames extends AsyncTask<String,Void,String> {

        ProgressDialog pdLoading = new ProgressDialog(ViewCustomers.this);
        HttpURLConnection conn;
        URL url = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... params) {
            try {

                url = new URL(ConstantValues.VIEW_CUSTOMERS_URL);

            } catch (MalformedURLException e) {

                e.printStackTrace();
                return e.toString();
            }
            try {

                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(ConstantValues.READ_TIMEOUT);
                conn.setConnectTimeout(ConstantValues.CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Adding data to be uploaded
                Uri.Builder builder = new Uri.Builder().appendQueryParameter("location", locationName);
                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {
                    return("Connection error");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String result) {

            pdLoading.dismiss();
            customerNames = new ArrayList<>();

            pdLoading.dismiss();

            if(result.equals("[]"))
            {
                Toast.makeText(ViewCustomers.this,"There is no customer in this location",Toast.LENGTH_LONG).show();
            }

            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    Customer data = new Customer(json_data.getString("name"));

                    customerNames.add(data);

                }

                // Setup and Handover data to recyclerview
                recyclerView = findViewById(R.id.rv_customers);
                recyclerView.setHasFixedSize(true);
                customerAdapter = new CustomerAdapter(ViewCustomers.this, customerNames);
                recyclerView.setAdapter(customerAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(ViewCustomers.this));





            } catch (JSONException e) {
                Toast.makeText(ViewCustomers.this, "An error has occurred", Toast.LENGTH_LONG).show();

            }

        }

    }
}
