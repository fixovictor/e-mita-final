package android.example.com.e_mita.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.example.com.e_mita.R;
import android.example.com.e_mita.utils.ConstantValues;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class StatisticsActivity extends AppCompatActivity {
    private TextView completedTasks, incompleteTasks;
    private String email;
    private ProgressDialog progressDialog;
    private ImageView refreshButton, profileImageMenu;
    private SharedPreferences mPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        Toolbar toolbar = findViewById(R.id.toolbar_statistics);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        profileImageMenu = findViewById(R.id.profile_image_menu_statistics);
        profileImageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view);
            }
        });

        mPreferences = getSharedPreferences(ConstantValues.sharedPrefFile, MODE_PRIVATE);
        loadProfileImage();

        refreshButton = findViewById(R.id.refresh_statistics);
        completedTasks = findViewById(R.id.numberOfCompletedTasks);
        incompleteTasks = findViewById(R.id.numberOfIncompleteTasks);

        //getting email address of logged on user to retrieve tasks assigned to them
        SharedPreferences sharedPreferences=getSharedPreferences("Login_Email", Context.MODE_PRIVATE);
        email=sharedPreferences.getString("email","");

        getCompletedTasks();
        getIncompleteTasks();

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCompletedTasks();
                getIncompleteTasks();
            }
        });
    }

    public void loadProfileImage(){
        String profileUrl = mPreferences.getString(ConstantValues.PROFILE_IMAGE_URI, "");
        if(profileUrl.equals("")){
            profileImageMenu.setImageResource(R.drawable.ic_account_white);

        }else{
            Picasso.with(this).load(profileUrl)
                    .transform(new CropCircleTransformation())
                    .into(profileImageMenu);
        }
    }

    // Display anchored popup menu based on view selected
    private void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        // Inflate the menu from xml
        popup.inflate(R.menu.main_menu);
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_logout:
                        //sign out individual from app
                        AlertDialog.Builder alertDialog;
                        //sign out individual from app
                        alertDialog=new AlertDialog.Builder(StatisticsActivity.this);
                        alertDialog.setTitle("Logout Confirmation");
                        alertDialog.setMessage("Are you sure you want to logout?");
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent logout = new Intent(StatisticsActivity.this, LoginActivity.class);
                                startActivity(logout);
                                finish();

                            }
                        });
                        alertDialog.setNegativeButton("NO", null);
                        alertDialog.setCancelable(false);
                        alertDialog.show();

                        return true;
                    case R.id.action_profile:
                        //Open profile activity
                        Intent profile = new Intent(StatisticsActivity.this, ProfileActivity.class);
                        startActivity(profile);
                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    public void getCompletedTasks()
    {
        GetCompletedTasks getCompletedTasks=new GetCompletedTasks();
        getCompletedTasks.execute(email);

    }

    public void getIncompleteTasks()
    {
        GetIncompleteTasks getIncompleteTasks=new GetIncompleteTasks();
        getIncompleteTasks.execute(email);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(StatisticsActivity.this,DashboardActivity.class);
        startActivity(intent);
    }

    @SuppressLint("StaticFieldLeak")
    private class GetCompletedTasks extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... voids) {
            try {
                URL url=new URL(ConstantValues.COMPLETED_TASKS_URL);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setReadTimeout(ConstantValues.READ_TIMEOUT);
                httpURLConnection.setConnectTimeout(ConstantValues.CONNECTION_TIMEOUT);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                //opening an output stream
                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data_to_post= URLEncoder.encode("email","UTF-8")+"="+URLEncoder.encode(email,"UTF-8");

                bufferedWriter.write(data_to_post);

                //closing open objects
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();


                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String line;
                StringBuilder result= new StringBuilder();

                while((line=bufferedReader.readLine())!=null)
                {
                    result.append(line);
                }

                //closing open connections
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            } catch (MalformedURLException e) {
                progressDialog.dismiss();
                Toast.makeText(StatisticsActivity.this, "Connection error, please refresh", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            } catch (IOException e) {
                progressDialog.dismiss();
                Toast.makeText(StatisticsActivity.this, "Connection error, please refresh", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {

            progressDialog = ProgressDialog.show(StatisticsActivity.this,"Loading","Please Wait",false,false);
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            try
            {
                completedTasks.setText(result);
            }catch(NullPointerException e)
            {
                Toast.makeText(StatisticsActivity.this, "Connection error, please refresh", Toast.LENGTH_LONG).show();
            }


        }
    }

    @SuppressLint("StaticFieldLeak")
    private class GetIncompleteTasks extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... voids) {
            try {
                URL url=new URL(ConstantValues.INCOMPLETE_TASKS_URL);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                //opening an output stream
                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data_to_post= URLEncoder.encode("email","UTF-8")+"="+URLEncoder.encode(email,"UTF-8");

                bufferedWriter.write(data_to_post);

                //closing open objects
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String line;
                StringBuilder result= new StringBuilder();

                while((line=bufferedReader.readLine())!=null)
                {
                    result.append(line);
                }

                //closing open connections
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            } catch (MalformedURLException e) {

                e.printStackTrace();
            } catch (IOException e) {

                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            incompleteTasks.setText(result);

        }
    }
}
