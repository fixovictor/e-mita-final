package android.example.com.e_mita.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.example.com.e_mita.R;
import android.example.com.e_mita.utils.ConnectivityReceiver;
import android.example.com.e_mita.utils.ConstantValues;
import android.example.com.e_mita.utils.MyApplication;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class LoginActivity extends AppCompatActivity implements
        ConnectivityReceiver.ConnectivityReceiverListener{

    private TextView forgotPass;
    private EditText email, password;
    private ImageView hidePass, showPass;
    private String emailText, passwordText;
    private ProgressBar progressBar;
    private Button login;
    public static final String MyPREFERENCES="Login_Email";
    private AlertDialog.Builder alertDialog;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //View decorView = getWindow().getDecorView();
        // Hide the status bar.
        //int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        //decorView.setSystemUiVisibility(uiOptions);

        forgotPass = findViewById(R.id.tv_forgot_password);
        email = findViewById(R.id.ev_email);
        password = findViewById(R.id.ev_password);
        hidePass = findViewById(R.id.hide_password);
        showPass = findViewById(R.id.show_password);
        progressBar = findViewById(R.id.progress_login);
        login = findViewById(R.id.login_button);

        forgotPass.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent forgot = new Intent (LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(forgot);
            }
        });


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailText=email.getText().toString();
                passwordText=password.getText().toString();
                if (!passwordText.isEmpty() && !emailText.isEmpty())
                {
                    AuthenticateUser authenticateUser=new AuthenticateUser();
                    authenticateUser.execute(emailText,passwordText);
                }
                else
                {
                    alertDialog=new AlertDialog.Builder(LoginActivity.this);
                    alertDialog.setMessage("Do not leave any field empty");
                    alertDialog.setPositiveButton("OK",null);
                    alertDialog.setCancelable(false);
                    alertDialog.show();
                }

            }
        });

    }

    public void loginUser(View view) {
        //Mannually checking internet connection
        Intent dash = new Intent(LoginActivity.this, DashboardActivity.class);
      //  startActivity(dash);
        //checkConnection();

        //Authenticate user
        //emailText = email.getText().toString();
        //passwordText = password.getText().toString();

        //new AuthenticateUser().execute(emailText, passwordText);
    }

    @SuppressLint("StaticFieldLeak")
    private class AuthenticateUser extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... voids) {
            try {
                URL url=new URL(ConstantValues.LOGIN_URL);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setReadTimeout(ConstantValues.READ_TIMEOUT);
                httpURLConnection.setConnectTimeout(ConstantValues.CONNECTION_TIMEOUT);

                //opening an output stream
                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data_to_post= URLEncoder.encode("email","UTF-8")+"="+URLEncoder.encode(emailText,"UTF-8")+"&"
                        + URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(passwordText,"UTF-8");
                bufferedWriter.write(data_to_post);

                //closing open objects
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                Log.i("Data","Message"+data_to_post);

                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String line;
                String result="";

                while((line=bufferedReader.readLine())!=null)
                {
                    result+=line;
                }

                //closing open connections
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                progressDialog.dismiss();
                e.printStackTrace();
            } catch (IOException e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            alertDialog=new AlertDialog.Builder(LoginActivity.this);
            alertDialog.setTitle("Login status");
            progressDialog = ProgressDialog.show(LoginActivity.this,"Verifying your credentials","Please Wait",false,false);
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            alertDialog.setMessage(result);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("OK",null);

            try {
                if(result.contains("Login Successful"))
                {
                    String name=email.getText().toString();
                    email.setText("");
                    password.setText("");
                    //creating shared preferences to store email that will be used in Tasks class
                    SharedPreferences sharedPreferences=getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putString("email",name);
                    editor.apply();

                    //intent to go to dashboard when login is successful
                    Intent intent=new Intent(LoginActivity.this,DashboardActivity.class);
                    startActivity(intent);

                }
                else
                {
                    alertDialog.show();
                }

            }catch (NullPointerException e)
            {
                checkConnection();
                Toast.makeText(LoginActivity.this,"Connection error. Please try again",Toast.LENGTH_LONG).show();
            }

        }
    }

    //Method to manually check connection status
    private void checkConnection(){
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    //Showing the status in Snackbar
    private void showSnack(boolean isConnected){
        if(!isConnected){
            String message = "No internet connection";
            int color = Color.CYAN;

            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.tv_forgot_password), message, Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }

    }

    protected void onResume(){
        super.onResume();
        //register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    public void revealPasswordCharacters(View view) {
        hidePass.setVisibility(View.INVISIBLE);
        showPass.setVisibility(View.VISIBLE);

        password.setTransformationMethod(null);
    }

    public void hidePasswordCharacters(View view) {
        showPass.setVisibility(View.INVISIBLE);
        hidePass.setVisibility(View.VISIBLE);

        password.setTransformationMethod(new PasswordTransformationMethod());
    }
}
